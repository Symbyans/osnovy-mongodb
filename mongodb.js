//------------------------- Mongodb -----------------------//


const MongoClient = require('mongodb').MongoClient;
const url 				= 'mongodb://localhost:27017/my_database_name';
//----------------


MongoClient.connect( url, function( err, db ) {
	if( err ) {
		console.log( 'Mongo data base is not difined. Error: ', err );
	} else {
		console.log( 'Connecting is succesfully on url: ', url );

		let collection 	= db.collection('users');

		let user_0			=	{	
												'name': 'Tanya', 
												'gender': 'w', 
												'age': 19
										 	};
		let user_1			=	{	
												'name': 'Gena',
											 	'gender': 'm' 
											};
		let user_2			=	{	
												'name': 'Nastya', 
												'age': 31 
											};
		let user_3			=	{	'name': 'Tanya', 
												'gender': 'w',
												'age': 21 
											};
		let user_4			=	{	
												'name': 'Petya',
											 	'age': 35 
											 };

//------------------------

		collection.insert([ user_0, user_1, user_2, user_3, user_4 ], function( err, result ) {
			if( err ){
				console.log(err);
			} else {
				console.log('Users is sucessfully added');
				console.log('-----'.repeat(10));
				console.log('Users list: ',result);
				console.log('-----'.repeat(10));
			}
		});
//------------------------
		
		collection.update({name: 'Gena'}, {$set: {name: 'Denis'}});
		collection.update({gender: 'w'}, {$set: {name: 'Maria'}}, {multi: true});

//------------------------

		collection.find().toArray(function( err, result ) {
			if( err ) {
				console.log( err );
			} else {
				console.log('-----'.repeat(10));
				console.log(result);
				console.log('-----'.repeat(10));
			}
		});

// //------------------------

		collection.remove({name : 'Gena'}, true);// удалил только имя
		collection.remove({name : 'Maria'}, false);// удалил полностью все элементы
		console.log('-----'.repeat(10));
// //--------------------------------------------------
// collection.drop();
db.close();
}
});






//----------------